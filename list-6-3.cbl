       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LIST6-3.
       AUTHOR. SIRARAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  REF-COUNT      PIC 9(4).
       01  PRN-REP-COUNT  PIC z,zz9.
       01  NUMBER-OF-TIMES   PIC 9(4) VALUE 1000.

       PROCEDURE DIVISION .
       000-BEGIN.
           PERFORM VARYING REF-COUNT FROM 0 BY 50
              UNTIL REF-COUNT = NUMBER-OF-TIMES 
              MOVE REF-COUNT TO PRN-REP-COUNT 
              DISPLAY "counting" PRN-REP-COUNT 

           END-PERFORM
           MOVE REF-COUNT TO PRN-REP-COUNT 
           DISPLAY "IF i have told you once,"
           DISPLAY "I've told you " PRN-REP-COUNT  " times."
           GOBACK
       .

